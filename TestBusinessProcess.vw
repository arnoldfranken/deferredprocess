Use Windows.pkg
Use DFClient.pkg
Use Batchdd.pkg
Use DeferredProcess.pkg

Deferred_View Activate_oTestBusinessProcess for ;
Object oTestBusinessProcess is a dbView

    Set Border_Style to Border_Thick
    Set Size to 200 300
    Set Location to 2 2
    Set Label to "TestBusinessProcess"

    Object oButton1 is a Button
        Set Location to 58 126
        Set Label to 'oButton1'
    
        // fires when the button is clicked
        Procedure OnClick
            Send Execute_oTestBP Self
        End_Procedure

        Procedure Initialize_oTestBP Handle hoProcess
            Integer iEnd
            Move (Random(10)) to iEnd
            Set piEnd of hoProcess to iEnd
        End_Procedure
    
    End_Object

Cd_End_Object

Deferred_Process Execute_oTestBP for;
Object oTestBP is a BusinessProcess
    Set Process_Title to "Test BusinessProcess"
    Set Process_Message to "Counter"
    
    Property Integer piEnd
    
    Procedure OnProcess
        Integer iCounter iEnd
        
        Get piEnd to iEnd
        
        For iCounter from 1 to iEnd
            Send Update_Status iCounter
            Sleep 1
        Loop
    End_Procedure
Cd_End_Object    
